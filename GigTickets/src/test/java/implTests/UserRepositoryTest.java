package implTests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;

import java.sql.Connection;
import java.sql.DriverManager;

import org.junit.Test;

import repositories.IRepository;
import repositories.impl.UserRepository;
import unitofwork.IUnitOfWork;
import unitofwork.UnitOfWork;
import domain.User;
import entityBuilders.IEntityBuilder;
import entityBuilders.UserEntityBuilder;

public class UserRepositoryTest
{

    @Test
    public void testPrepareUpdateQuery() throws Exception 
    {
        try 
        {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/gigtickets", "root","");
        IEntityBuilder<User> builder = new UserEntityBuilder();
        IUnitOfWork uow = new UnitOfWork(connection);
        IRepository<User> repo = new UserRepository(connection,builder, uow);
        User u = new User();
        u.setId(8);
        u.setLogin("Kasia");                     
        u.setPassword("qwerty");
        repo.add(u);
        assertNotNull(u);
        User u1 = new User();
        u1.setLogin("Basia");                  
        u1.setPassword("pass");
        repo.add(u1);
        u.setLogin("Lisc");
        repo.update(u);
        uow.commit();
        assertNotSame(repo.get(8), repo.get(10));
        System.out.println(u.getLogin());
        connection.close();
        } 
        catch (Exception e) 
        {
        e.printStackTrace();
        }
}

    @Test
    public void testPrepareAddQuery() throws Exception 
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/gigtickets", "root","");
            IEntityBuilder<User> builder = new UserEntityBuilder();
            IUnitOfWork uow = new UnitOfWork(connection);
            IRepository<User> repo = new UserRepository(connection, builder, uow);
            User p = new User();
            p.setLogin("Rel");
            p.setPassword("qwerty");
            repo.add(p);
            uow.commit();
            connection.close();
        } 
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}