package repositories;

import domain.User;
import domain.Person;

public interface IUserRepository extends IRepository<User> {
    
    public User getUserByLogin(String login);
    public User getUserByEmail(String email);
	public User getUserByPerson(Person person);
    
}