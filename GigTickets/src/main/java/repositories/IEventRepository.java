package repositories;

import java.util.*;

import domain.Event;

public interface IEventRepository extends IRepository<Event> {
	
	List<Event> searchByTitle(String title); 
	List<Event> searchByDate(String date);
	List<Event> searchByVenue(String venue);
	List<Event> searchByCity(String city);
	List<Event> searchByPrice(String price);

}
