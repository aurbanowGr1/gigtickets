package repositories;

import java.util.List;

import domain.Event;
import domain.User;

public interface IUserRepositoryDummy extends IRepositoryDummy<User> {

	public List<User> withEvent(Event event);
	public List<User> withEvent(String eventName);
	public List<User> withEvent(int eventId);
}