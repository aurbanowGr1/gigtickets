package repositories;

import domain.*;

public interface IRepositoryCatalog {

	public IRepository<Person> getPersons();
	public IRepository<User> getAddresses();
	public IRepository<Address> getUsers();
	public IRepository<Event> getEvents();
}
