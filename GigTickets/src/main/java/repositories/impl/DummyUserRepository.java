package repositories.impl;

import java.util.List;

import domain.Event;
import domain.User;
import domain.Person;
import repositories.IUserRepositoryDummy;

public class DummyUserRepository implements IUserRepositoryDummy{

	private DummyDb db;
	
	public DummyUserRepository(DummyDb db) {
		super();
		this.db = db;
	}

	@Override
	public User get(int id) {
		for(User u : db.users)
		{
			if(u.getId()==id)
				return u;
		}
		return null;
	}

	@Override
	public List<User> getAll() {
		return db.users;
	}

	@Override
	public void add(User entity) {

		db.users.add(entity);
		
	}

	@Override
	public void delete(User entity) {

		db.users.remove(entity);
		
	}

	@Override
	public void update(User entity) {
		
	}

	@Override
	public List<User> withEvent(Event event) {
		
		return withEvent(event.getId());
	}

	@Override
	public List<User> withEvent(String eventTitle) {

		Event event = null;
		for(Event e : db.events)
		{
			if(e.getTitle().equalsIgnoreCase(eventTitle))
			{
				event = e;
				break;
			}
		}
		if(event==null)return null;
		return event.getUsers();
	}

	@Override
	public List<User> withEvent(int eventId) {

		Event event = null;
		for(Event e : db.events)
		{
			if(e.getId()==eventId)
			{
				event = e;
				break;
			}
		}
		if(event==null)return null;
		return event.getUsers();
	}

}