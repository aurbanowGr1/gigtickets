package repositories.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.Entity;
import domain.Person;
import entityBuilders.IEntityBuilder;
import repositories.IRepository;
import unitofwork.IUnitOfWork;

public class PersonRepository
	extends RepositoryBase<Person>{

	public PersonRepository(Connection connection, IEntityBuilder<Person> builder, IUnitOfWork uow) {
	
		super(connection, builder, uow);
	}

	@Override
	protected String getTableName() {
		return "person";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE person SET (firstname,surname,pesel,email)=(?,?,?,?) WHERE id=?";
	}

	@Override
	protected String getCreateQuery() {
		return "Insert into person(firstname,surname,email,pesel) values (?,?,?,?)";
	}

	@Override
	protected void prepareAddQuery(Person entity) throws SQLException {
		save.setString(1, entity.getFirstName());
		save.setString(2, entity.getLastName());
		save.setInt(3, entity.getPhone());
		save.setString(4, entity.getEmail());
		
		
	}


	@Override
	protected void prepareUpdateQuery(Person entity) throws SQLException {
		update.setString(1, entity.getFirstName());
		update.setString(2, entity.getLastName());
		update.setInt(4, entity.getPhone());
		update.setString(3, entity.getEmail());
		update.setLong(5, entity.getId());
		
	}
	
}
