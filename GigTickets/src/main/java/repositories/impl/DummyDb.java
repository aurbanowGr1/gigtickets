package repositories.impl;

import java.util.*;

import domain.*;

public class DummyDb {

	public List<Person> persons;
	public List<User> users;
	public List<Address> address;
	public List<Event> events;
	
	public DummyDb()
	{
		persons = new ArrayList<Person>();
		users = new ArrayList<User>();
		events =new ArrayList<Event>();
		address = new ArrayList<Address>();
	}
	
}