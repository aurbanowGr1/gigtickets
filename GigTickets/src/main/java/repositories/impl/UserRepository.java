package repositories.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.Person;
import domain.User;
import entityBuilders.IEntityBuilder;
import repositories.IRepository;
import unitofwork.IUnitOfWork;

public class UserRepository
	extends RepositoryBase<User>
{

	public UserRepository(Connection connection, IEntityBuilder<User> builder, IUnitOfWork uow) {
	
		super(connection,builder, uow);
	}

	@Override
	protected String getTableName() {

		return "user";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE user SET (login,password)=(?,?) WHERE id=?";
	}

	@Override
	protected String getCreateQuery() {
		// TODO Auto-generated method stub
		return "INSERT INTO user(login,password) VALUES (?,?)";
	}

	@Override
	protected void prepareAddQuery(User entity) throws SQLException {
		save.setString(1, entity.getLogin());
        save.setString(2, entity.getPassword());
	}

	@Override
	protected void prepareUpdateQuery(User entity) throws SQLException {
		update.setString(1, entity.getLogin());
        update.setString(2, entity.getPassword());
        update.setLong(3, entity.getId());
	}
}
