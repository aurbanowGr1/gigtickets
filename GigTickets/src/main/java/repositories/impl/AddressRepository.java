package repositories.impl;

import java.sql.Connection;
import java.sql.SQLException;

import unitofwork.IUnitOfWork;
import domain.Address;
import entityBuilders.IEntityBuilder;

public class AddressRepository extends RepositoryBase<Address> {

	protected AddressRepository(Connection connection,
			IEntityBuilder<Address> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
	}

	@Override
	protected void prepareUpdateQuery(Address entity) throws SQLException {
	
		
	}

	@Override
	protected void prepareAddQuery(Address entity) throws SQLException {
		
		
	}

	@Override
	protected String getTableName() {
		return "addres";
	}

	@Override
	protected String getUpdateQuery() {
		return null;
	}

	@Override
	protected String getCreateQuery() {
		return null;
	}

}
