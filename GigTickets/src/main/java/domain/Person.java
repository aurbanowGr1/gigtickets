package domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Person extends Entity {
	
	public Person()
	{
		this.address=new ArrayList<Address>();
	}
	
	private User user;
	private String firstName;
	private String lastName;
	private List<Address> address;
	private Date dateOfBirth = new Date();
	private int phone;
	private String email;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public List<Address> getAddress() {
		return address;
	}

	public void setAddresses(List<Address> address) {
		this.address = address;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public int getPhone() {
		return phone;
	}
	public void setPhone(int phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	

}
