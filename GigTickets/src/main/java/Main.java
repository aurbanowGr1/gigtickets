import java.sql.Connection;
import java.sql.DriverManager;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import domain.Person;
import entityBuilders.IEntityBuilder;
import entityBuilders.PersonEntityBuilder;
import repositories.IRepository;
import repositories.impl.PersonRepository;
import unitofwork.IUnitOfWork;
import unitofwork.UnitOfWork;


public class Main {

	public static void main(String[] args) {


		try 
		{
			
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection connection = DriverManager
					.getConnection("jdbc:mysql://localhost/test?user=newuser&password=1qaz2WSX");
			IEntityBuilder<Person> builder = new PersonEntityBuilder();
			IUnitOfWork uow = new UnitOfWork(connection);
			IRepository<Person> repo = 
					new PersonRepository(connection, builder, uow);
			Person p = new Person();
			p.setFirstName("jan");
			p.setLastName("Nowak");
			p.setPhone(123456789);
			repo.add(p);
			uow.commit();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("koniec");
	}

}
