package entityBuilders;

import java.sql.ResultSet;

import domain.User;

public class UserEntityBuilder implements IEntityBuilder<User>
{
	@Override
	public User build(ResultSet rs) {
		try
		{
			User u = new User();
			u.setLogin(rs.getString("login"));
            u.setPassword(rs.getString("password"));
			return u;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	} 

}
