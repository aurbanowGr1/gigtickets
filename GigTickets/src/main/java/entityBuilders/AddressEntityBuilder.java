package entityBuilders;

import java.sql.ResultSet;

import domain.Address;

public class AddressEntityBuilder implements IEntityBuilder<Address>
{
	@Override
	public Address build(ResultSet rs) {
		try
		{
			Address a = new Address();
			a.setCity(rs.getString("Gdynia"));
			a.setStreet(rs.getString("123456789"));
			a.setPostalCode(rs.getString("83-504"));
			a.setHouseNumber(rs.getString("22"));
			a.setLocalNumber(rs.getString("22"));
            return a;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	} 

}
