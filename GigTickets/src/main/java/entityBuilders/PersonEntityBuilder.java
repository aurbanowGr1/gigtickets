package entityBuilders;

import java.sql.ResultSet;

import domain.Person;

public class PersonEntityBuilder implements IEntityBuilder<Person>
{
	@Override
	public Person build(ResultSet rs) {
		try
		{
			Person p = new Person();
			p.setFirstName(rs.getString("firstName"));
			p.setLastName(rs.getString("lastName"));
			p.setPhone(rs.getInt("phone"));
			p.setEmail("email");
			return p;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	} 

}
